import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing-module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { DetailPage } from './Pages/detail-page/detail.page';
import { LoginPage } from './Pages/login-page/login.page';
import { PokedexPage } from './Pages/pokedex-page/pokedex.page';
import { TrainerPage } from './Pages/trainer-page/trainer.page';
import { PokeCardComponent } from './Components/poke-card/poke-card.component';
import { SelectedCardComponent } from './Components/selected-card/selected-card.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    TrainerPage,
    PokedexPage,
    DetailPage,
    PokeCardComponent,
    SelectedCardComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

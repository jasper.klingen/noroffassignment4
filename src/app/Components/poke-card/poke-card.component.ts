import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { PokemonListItem } from 'src/app/services/models/pokemon-list-item';
import { PokemonService } from 'src/app/services/pokemon.service';
import { PokedexPage } from '../../Pages/pokedex-page/pokedex.page'

@Component ({
    selector: 'app-poke-card',
    templateUrl: './poke-card.component.html',
    styleUrls: ['./poke-card.component.css']
})

export class PokeCardComponent{

    
    @Input() pokeCardInfo: PokemonListItem | undefined;
    @Output() onClick : EventEmitter<PokemonListItem> = new EventEmitter()

    public onPokeCardClicked(): void{
        console.log("clicked")
        this.onClick.emit(this.pokeCardInfo)
    }
}
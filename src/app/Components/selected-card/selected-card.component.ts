import { Component, Input, Type } from '@angular/core'
import { Observable } from 'rxjs';
import { PokeDetail, Types } from 'src/app/services/models/pokemon-list-item';
import { SelectedCardService } from 'src/app/services/selected-card.service';

@Component({
    selector: 'app-selected-card',
    templateUrl: './selected-card.component.html',
    styleUrls: ['./selected-card.component.css']
})

export class SelectedCardComponent {
    @Input() details: PokeDetail | undefined;
    constructor(){

    }

}

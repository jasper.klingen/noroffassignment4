import {Component, NgModule} from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { DetailPage } from './Pages/detail-page/detail.page'
import { LoginPage } from './Pages/login-page/login.page'
import { PokedexPage } from './Pages/pokedex-page/pokedex.page'
import { TrainerPage } from './Pages/trainer-page/trainer.page'
import { AuthGuard } from './services/authguard.service'

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/loginpage'
        
    },
    {
        path: 'loginpage',
        component: LoginPage
    },
    {
        path: 'trainerpage',
        component: TrainerPage
    },
    {
        path: 'pokedex',
        component: PokedexPage,
        canActivate: [AuthGuard]
    },
    {
        path: 'detail/:id',
        component: DetailPage
    }



]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
    
}
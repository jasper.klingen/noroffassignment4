import { Injectable } from "@angular/core";
import { PokemonListItem } from "./models/pokemon-list-item";

const STORAGE_KEY_NAME = 'latest-trainer';
const STORAGE_KEY_POKEMON = 'caught-pokemon';


@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  private _caughtPokemonState:PokemonListItem[] = [];
  private _loggedInTrainer: null | string = null;

  getLoggedInTrainer() {
    return this._loggedInTrainer;
  }

  loginTrainer(name: string ) {
    this._loggedInTrainer = name;

    localStorage.setItem(STORAGE_KEY_NAME, name);
  }

  getLatestTrainer(): string | null {
    return localStorage.getItem(STORAGE_KEY_NAME);
  }

  storeCaughtPokemon (pokemon: PokemonListItem[]) {
    localStorage.setItem(STORAGE_KEY_POKEMON, JSON.stringify(pokemon));
  }

  getCaughtPokemon (): PokemonListItem[] | null {
    return JSON.parse(localStorage.getItem(STORAGE_KEY_POKEMON) || "") || null;
  }

  setCaughtPokemonState (pokemon: PokemonListItem){
      this._caughtPokemonState.push(pokemon)
      this.storeCaughtPokemon(this._caughtPokemonState)
  }}
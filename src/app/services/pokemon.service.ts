import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { PokemonList, PokemonListItem } from './models/pokemon-list-item'


@Injectable({
    providedIn: 'root'
})
export class PokemonService {
    private _pokemonList: PokemonListItem[] = [];
    private _error: string = '';

    constructor(private readonly http: HttpClient) {

    }

    public fetchPokemonList(): void {
        this.http.get<PokemonList>('https://pokeapi.co/api/v2/pokemon/?offset=0&limit=150')
        .subscribe(pokemon => {
            const {results} = pokemon;
            this._pokemonList = this.extendPokemonObject(results);
        }, 
        (error: HttpErrorResponse) => {
            console.log(this._error)
        })
    }
  
    public pokemon(): PokemonListItem[]{
        return this._pokemonList;
    }

    public getSpecificPokemon(id = 1): PokemonListItem | undefined {
        return this._pokemonList.find(p => p.id == id);
    }

    private extendPokemonObject(results: PokemonListItem[]): PokemonListItem[] {
        return results.map(listItem => {
            const id = parseInt(listItem.url.split('/').filter(Boolean).pop() || "");
            return {
                ...listItem,
                id,
                imgurl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`,
                name: listItem.name.charAt(0).toUpperCase() + listItem.name.slice(1)//first letter capitalize
            }
        });
    }
}  
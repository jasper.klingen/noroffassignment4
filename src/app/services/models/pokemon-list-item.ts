export interface PokemonList{
    results: PokemonListItem[];
}
export interface PokemonListItem{
    name: string;
    url: string;
    imgurl: string;
    id: number;
}

export interface PokeDetail {
    id: number;
    name: string;
    types: Types[];
    stats: Stats[];
    moves: Moves[];
    height: number;
    weight: number;
    abilities: Abilities[];
    base_experience: number;
}
export interface Stats{
    base_stat: number;
    stat: {
        name: string;
    }
}
//* needs to be deconstructed from original object in the service
export interface Types{
    type: {
        name: string;
    };
}
export interface Moves{
    move: {
        name: string;
    }
}
export interface Abilities{
    ability: {
        name: string;
    }
}



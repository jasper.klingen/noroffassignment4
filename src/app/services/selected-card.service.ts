import { Injectable } from '@angular/core'
import { PokemonListItem, Types, Stats, Moves, Abilities, PokeDetail} from './models/pokemon-list-item'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SelectedCardService {
    private _card: PokemonListItem | null = null;
    private _detailCard: PokeDetail | null = null;
    private _error: string = "";
    
    constructor(private readonly http: HttpClient) {}

    fetchPokemonDetails(id: number): Observable<PokeDetail> {
        return this.http.get<PokeDetail>(`https://pokeapi.co/api/v2/pokemon/${id}/`);//http request when detail page is loaded        
    }

    detailCard(): PokeDetail | null{
        return this._detailCard;
    }

    setSelectedCard(selectedCard: PokemonListItem){
        this._card = selectedCard;
    }

    selectedCard(): PokemonListItem | null {
        return this._card;
    }
}
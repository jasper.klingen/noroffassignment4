import { Component } from '@angular/core'
import { Router } from '@angular/router';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.css']
})
export class LoginPage {
    public trainerName: string | null = null;

    constructor(private trainerService: TrainerService, private router: Router) {}

    ngOnInit() {
        const latestTrainer = this.trainerService.getLatestTrainer();
        if (latestTrainer) {
            this.toTrainerPage();
        }
    }

    onClickLogin () {
        if(this.trainerName) {
            this.trainerService.loginTrainer(this.trainerName);
            this.toTrainerPage();

        } else {
            alert('please fill in a trainer name')
        }
    }

    toTrainerPage() {
        this.router.navigate(['./trainerpage']);
    }
}
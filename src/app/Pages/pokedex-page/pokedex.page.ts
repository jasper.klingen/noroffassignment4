import { Component, OnInit } from '@angular/core' 
import { PokemonList, PokemonListItem } from 'src/app/services/models/pokemon-list-item';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component ({
    selector: 'app-pokedex',
    templateUrl: './pokedex.page.html',
    styleUrls: ['./pokedex.page.css']

})

export class PokedexPage implements OnInit {
    constructor (private readonly pokemonService :  PokemonService){
        this.pokemon
    }
    get pokemon(): PokemonListItem[] | undefined {

        return this.pokemonService.pokemon();
    }
    ngOnInit() : void{
        this.pokemonService.fetchPokemonList();
        //this.pokemonService
    }

}
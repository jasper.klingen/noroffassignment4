import { Component } from '@angular/core'
import { PokemonListItem } from 'src/app/services/models/pokemon-list-item';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
    selector: 'app-trainer',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.css']
})

export class TrainerPage {
    constructor(private readonly trainer: TrainerService) {

    }
    get pokecards(): PokemonListItem[] | null{
        return this.trainer.getCaughtPokemon();
    }
    get trainerName(): string | null {
        return this.trainer.getLatestTrainer();
    }
}
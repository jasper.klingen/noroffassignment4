import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from "rxjs/operators";
import { PokeDetail, PokemonListItem } from 'src/app/services/models/pokemon-list-item';
import { PokemonService } from 'src/app/services/pokemon.service';
import { SelectedCardService } from 'src/app/services/selected-card.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { TrainerPage } from '../trainer-page/trainer.page';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.page.html',
    styleUrls: ['./detail.page.css']
})
export class DetailPage {
    public details: PokeDetail | undefined;
    public pokeCardInfo: PokemonListItem | undefined; 

    constructor(private route: ActivatedRoute, private selectedCardService: SelectedCardService, private pokemonService: PokemonService, private router: Router, private trainer: TrainerService) {}

    ngOnInit() {
        this.route.params.pipe(
            map(params => params['id']),
            switchMap((id) => this.selectedCardService.fetchPokemonDetails(id))
        )
        .subscribe(details => this.details = details)

        this.route.params.pipe(
            map(params => params['id']),
            map((id) => this.pokemonService.getSpecificPokemon(id))
        ).subscribe(cardInfo => this.pokeCardInfo = cardInfo)
    }
    onClickCatchPokemon(caughtPokemon: PokemonListItem | undefined){
        if(caughtPokemon){
            this.trainer.setCaughtPokemonState(caughtPokemon)
        }
        this.toTrainerPage();
    }
    public toTrainerPage() {
        this.router.navigate(['./trainerpage']);
    }
}